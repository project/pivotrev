﻿							PIVOTREV Module
=====================

This module acts as an interface of PIVOTREV.com. PIVOTREV is a cloud based, online centralized review and rating system for real consumers community portal with a new way of product reviews to find out what real users are saying about your products or services you want to sale.

For a full description visit the project page: http://drupal.org/project/pivotrev
Bug reports, feature suggestions and latest developments:
http://drupal.org/project/issues/pivotrev

REQUIREMENTS
------------

This module requires  Drupal 8.

INSTALLATION
----------
1. Download and navigate to Administration > Extend and enable the PIVOTREV module.
2. PIVOTREV reviews can be enabled for any entity sub-type  (for example, a content type, sidebar, footer etc.). 
3. On the Block Layout click one of the Place Block button where you want to add PIVOTREV reviews to be appeared. 
4. Look for PIVOTREV from the list and click Place Block button.
5. Enter your Access Token.  You can get your Access Token at https://pivotrev.com
   sub-type,you can enable disqus by adding a Disqus comments field.
6. Click Save block.

Additional Requirements
--------
Access Token is required to run PIVOTREV on your website. You can get your Access Token by signing up at https://pivotrev.com

MAINTAINERS
-----------
This module was created and sponsored by PIVOTREV, 

 * PIVTOREV- https:/pivotrev.com/
		