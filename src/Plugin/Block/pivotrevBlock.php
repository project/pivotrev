<?php
namespace Drupal\pivotrev\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockPluginInterface;
/**
 * Provides a 'Pivotrev' Block
 * @Block(
 * id = "block_pivotrev",
 * admin_label = @Translation("PIVOTREV"),
 * )
 */
class pivotrevBlock extends BlockBase implements BlockPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['access_token'] = array (
      '#type' => 'textfield',
      '#title' => $this->t('Access Token'),
      '#description' => $this->t('Enter your Access Token. Signup and get your Access Token at https://pivotrev.com/users'),
      '#default_value' => isset($config['accesstoken']) ? $config['accesstoken'] : '',
	  '#required' => TRUE,
    );
	
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('accesstoken', $form_state->getValue('access_token'));
  }
  public function build() {
    $config = $this->getConfiguration();
    if (!empty($config['accesstoken'])) {
      $accesstoken = $config['accesstoken'];
    }
    else {
      $accesstoken = $this->t('Access Token');
    }
   
    return array(
	
      '#markup' => $this->t("<div id='pivotrev_reviews'></div><script>var token='@accesstoken';var d = document, s = d.createElement('script'); s.src = 'https://api.pivotrev.com/embed.js';(d.head || d.body).appendChild(s);</script>", 
       array (
         '@accesstoken' => $accesstoken, 
       )
       ),
    );
  }
}